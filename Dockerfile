FROM node:7.0.0

WORKDIR /sails
EXPOSE 1337

RUN npm install -g sails
