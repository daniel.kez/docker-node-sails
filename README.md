# docker-node-sails

A [Sails](http://sailsjs.org) application

# Usage

Fork this repo and start developing right away with sails & node. Very simple setup:
1. Fork (or clone) this Repo. See more examples on how to duplicate this repo [here](https://help.github.com/articles/duplicating-a-repository/).
    
    ```
    git clone git@gitlab.com:daniel.kez/docker-node-sails.git my-project-directory-name
    ```
2. Install [docker](https://docs.docker.com/engine/installation/) and [docker-compose](https://docs.docker.com/compose/install/).
3. Run `docker-compose up` in your repo. The barebones sails app will be started on internal port 1337 and exposed to your host machine on port 13371 (You can change this in docker-compose.yml) 

    ```
    docker-compose up
    curl localhost:13371 # This should return a non-null response.
    ```
4. Start developing! Any changes made in the directory you started the application will be reflected in the running instance of the container. This is especially useful as it minimizes the number of host machine requirements & dependecies. Using this method you don't have to have sails installed on your host machine!

# Additional thoughts

At the moment this will always pull the latest version of sails from npm when the image is built. If you want to pull a newer version then you will have to delete the imaged cached in your local repository (`docker images`).
